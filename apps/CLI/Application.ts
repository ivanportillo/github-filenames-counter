import { command, positional, run, string } from 'cmd-ts';
import { GetFilenameCounts } from "../../src/application/GetFilenameCounts";
import { GithubFileExplorer } from "../../src/infrastructure/GithubFileExplorer";

export class Application {
  private getFilenameCounts: GetFilenameCounts;
  constructor() {
    const fileExplorer = new GithubFileExplorer();
    this.getFilenameCounts = new GetFilenameCounts(fileExplorer);
  }

  async run() {
    const cmd = command({
      name: 'githubFilenamesCounter',
      description: 'print a count of the words used in the classnames of a repo',
      version: '1.0.0',
      args: {
        owner: positional({ type: string, displayName: 'owner' }),
        repository: positional({ type: string, displayName: 'repository' }),
      },
      handler: args => {
        this.getFilenameCounts.run({
          user: args.owner,
          repository: args.repository
        }).then(count => console.log(count))
      },
    });

    run(cmd, process.argv.slice(2));
  }
}
