# github-filenames-counter

## Getting started
This is a project solving the [Github filenames counter](https://zinklar.notion.site/GitHub-filenames-counter-c1826a258be849328191845d16f5f053) problem. Follow the following steps to launch the project:
- `yarn install`
- `yarn run start <owner> <repository>`
    - E.g. `yarn run start symfony symfony`
- Enjoy! 🎸

## Running tests
- Run a single test `yarn run test:watch <relativePath>`
- Run the entire suite `yarn run test`