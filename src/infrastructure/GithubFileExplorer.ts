import { Octokit } from "octokit";
import { Content } from "../domain/Content";
import { FileExplorer } from "../domain/FileExplorer";
import { FileExplorerError } from '../domain/FileExplorerError';

export class GithubFileExplorer implements FileExplorer {
  private githubClient: Octokit;

  constructor() {
    this.githubClient = new Octokit({});
  }

  async getContent(params: {
    owner: string;
    repository: string;
    path?: string;
  }) {
    const { owner, repository, path } = params;
    try {
      const { data } = await this.githubClient.rest.repos.getContent({
        owner,
        repo: repository,
        path: path || "",
      });
  
      if (Array.isArray(data)) {
        return data.map(this.toEntity);
      }
  
      return Content.file(data.path);
    } catch(error) {
      throw new FileExplorerError(`Error fetching ${owner}/${repository}/${path || ''}: ${error}`)
    } 
  }

  private toEntity(contentData: { type: string; path: string }) {
    if (contentData.type === "file") {
      return Content.file(contentData.path);
    }

    return Content.folder(contentData.path);
  }
}
