import { Content } from "../domain/Content";
import { FileExplorer } from "../domain/FileExplorer";

const PASCAL_CASE_REGEX = /([A-Z][a-z]+)+?(?=[A-Z|\.][a-z]+)/g;

export class GetFilenameCounts {
  constructor(private fileExplorer: FileExplorer) {}

  async run(params: { user: string; repository: string }) {
    const rootContent: Array<Content> = await this.getContentFromFolder(params);

    const filenames = rootContent.map(this.getWordsFromFile);

    return this.mapFilenamesToObject(filenames.flat());
  }

  private mapFilenamesToObject(filenames: Array<string>) {
    return filenames.reduce((acc, filename) => {
      if (acc[filename]) {
        return {
          ...acc,
          [filename]: acc[filename] + 1,
        };
      }

      return { ...acc, [filename]: 1 };
    }, {} as { [key: string]: number });
  }

  private getWordsFromFile(content: Content) {
    return (content.path.match(PASCAL_CASE_REGEX) as Array<string>) || [];
  }

  private async getContentFromFolder(data: {
    content?: Content;
    repository: string;
    user: string;
  }): Promise<any> {
    const content = await this.fileExplorer.getContent({
      path: data?.content?.path,
      repository: data.repository,
      owner: data.user,
    });

    const thereIsMoreFolders =
      Array.isArray(content) &&
      !!content.find((contentElement) => contentElement.isFolder());
    if (!thereIsMoreFolders) {
      return content;
    }

    const getExecutions = content.map(async (contentElement) => {
      if (contentElement.isFolder()) {
        return await this.getContentFromFolder({
          content: contentElement,
          repository: data.repository,
          user: data.user,
        });
      } else {
        return contentElement;
      }
    });

    const receivedContent = await Promise.all(getExecutions);
    return receivedContent.flat();
  }
}
