export class FileExplorerError extends Error {
  constructor(reason: string) {
    super(`[FileExplorerError] ${reason}`);
  }
}
