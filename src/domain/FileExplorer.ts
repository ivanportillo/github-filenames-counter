import { Content } from "./Content";

export interface FileExplorer {
  getContent(params: {
    owner: string;
    repository: string;
    path?: string;
  }): Promise<Array<Content> | Content>;
}
