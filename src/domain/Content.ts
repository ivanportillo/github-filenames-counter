export class Content {
  path: string;
  private _isFolder: boolean;

  private constructor(params: { path: string; isFolder: boolean }) {
    this.path = params.path;
    this._isFolder = params.isFolder;
  }

  static file(path: string) {
    return new Content({
      isFolder: false,
      path,
    });
  }

  static folder(path: string) {
    return new Content({
      isFolder: true,
      path,
    });
  }

  isFile() {
    return !this._isFolder;
  }

  isFolder() {
    return this._isFolder;
  }
}
