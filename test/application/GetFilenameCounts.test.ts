import faker from "@faker-js/faker";
import { GetFilenameCounts } from "../../src/application/GetFilenameCounts";
import { Content } from "../../src/domain/Content";
import { FileExplorerDouble } from "../doubles/FileExplorerDouble";

describe("GetFilenameCounts application service", () => {
  it("counts the filenames of a repository with only files", async () => {
    const onGet = new Map().set(undefined, [
      Content.file("Service.php"),
      Content.file("MainController.php"),
    ]);
    const fileExplorer = new FileExplorerDouble({
      onGet,
    });
  
    const applicationService = new GetFilenameCounts(fileExplorer);

    const count = await applicationService.run({
      user: faker.random.word(),
      repository: faker.random.word(),
    });

    expect(count).toStrictEqual({
      Service: 1,
      Main: 1,
      Controller: 1,
    });
  });

  it("counts the filenames of a repository with files and folders", async () => {
    const onGet = new Map().set(undefined, [
      Content.file("Service.php"),
      Content.file("MainController.php"),
      Content.folder("tests/"),
    ]).set("tests/", [
      Content.file("TestService.php"),
      Content.file("TestController.php"),
    ]);
    const fileExplorer = new FileExplorerDouble({
      onGet,
    });

    const applicationService = new GetFilenameCounts(fileExplorer);

    const count = await applicationService.run({
      user: faker.random.word(),
      repository: faker.random.word(),
    });

    expect(count).toStrictEqual({
      Test: 2,
      Service: 2,
      Main: 1,
      Controller: 2
    });
  });

  it("counts the filenames of a repository without files to count in root", async () => {
    const onGet = new Map().set(undefined, [
      Content.file('package.json'),
      Content.folder("tests/"),
      Content.folder("src/"),
    ]).set("tests/", [
      Content.file("TestService.php"),
      Content.file("TestController.php"),
    ]).set("src/", [
      Content.file("Service.php"),
      Content.file("MainController.php"),
    ]);
    const fileExplorer = new FileExplorerDouble({
      onGet,
    });

    const applicationService = new GetFilenameCounts(fileExplorer);

    const count = await applicationService.run({
      user: faker.random.word(),
      repository: faker.random.word(),
    });

    expect(count).toStrictEqual({
      Test: 2,
      Service: 2,
      Main: 1,
      Controller: 2
    });
  });
});

