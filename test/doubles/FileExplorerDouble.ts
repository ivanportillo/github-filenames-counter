import { Content } from "../../src/domain/Content";
import { FileExplorer } from "../../src/domain/FileExplorer";

export class FileExplorerDouble implements FileExplorer {
  private onGet?: Map<string | undefined, Content | Array<Content>>;

  constructor(params?: {
    onGet?: Map<string | undefined, Content | Array<Content>>;
  }) {
    this.onGet = params?.onGet;
  }

  async getContent(params: {
    owner: string;
    repository: string;
    path?: string;
  }): Promise<Content | Content[]> {
    return this.onGet?.get(params.path) || [];
  }
}
