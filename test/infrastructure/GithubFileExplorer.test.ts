import nock from "nock";
import faker from "@faker-js/faker";
import { Content } from "../../src/domain/Content";
import { FileExplorerError } from "../../src/domain/FileExplorerError";
import { GithubFileExplorer } from "../../src/infrastructure/GithubFileExplorer";
import { fileResponse, notFoundResponse, tldrawRepositoryResponse } from "./tldrawRepositoryResponse";

describe("GithubFileExplorer", () => {
  afterAll(() => {
    nock.restore();
  });

  it("gets the content of a repository", async () => {
    const owner = "tldraw";
    const repository = "tldraw";
    nock("https://api.github.com")
      .get(`/repos/${owner}/${repository}/contents/`)
      .reply(200, tldrawRepositoryResponse);

    const fileExplorer = new GithubFileExplorer();

    const content = await fileExplorer.getContent({
      owner,
      repository,
    });

    const expectedContent = [
      Content.file(".eslintignore"),
      Content.file(".eslintrc"),
      Content.folder(".github"),
      Content.file(".gitignore"),
      Content.folder(".husky"),
      Content.file(".npmignore"),
      Content.file(".prettierrc"),
      Content.folder(".vscode"),
      Content.file("CODE_OF_CONDUCT.md"),
      Content.file("CONTRIBUTING.md"),
      Content.file("LICENSE.md"),
      Content.file("README.md"),
      Content.folder("apps"),
      Content.folder("assets"),
      Content.folder("examples"),
      Content.folder("guides"),
      Content.file("lerna.json"),
      Content.file("package.json"),
      Content.folder("packages"),
      Content.file("repo-map.tldr"),
      Content.file("setupTests.ts"),
      Content.file("tsconfig.base.json"),
      Content.file("tsconfig.json"),
      Content.file("yarn.lock"),
    ];
    expect(content).toStrictEqual(expectedContent);
  });

  it('gets the content when receives the path of a file', async () => {
    const owner = "tldraw";
    const repository = "tldraw";
    const path = '.gitignore';

    nock("https://api.github.com")
      .get(`/repos/${owner}/${repository}/contents/${path}`)
      .reply(200, fileResponse);

    const fileExplorer = new GithubFileExplorer();

    const content = await fileExplorer.getContent({
      owner,
      repository,
      path
    });

    const expectedContent = Content.file('.gitignore');
    expect(content).toEqual(expectedContent);
  })

  it('throws an exception when there is an error fetching data', async () => {
    const owner = faker.random.word();
    const repository = faker.random.word();

    nock("https://api.github.com")
      .get(`/repos/${owner}/${repository}/contents/`)
      .reply(404, notFoundResponse);

    const fileExplorer = new GithubFileExplorer();

    await expect(fileExplorer.getContent({
      owner,
      repository,
    })).rejects.toThrowError(new FileExplorerError(`Error fetching ${owner}/${repository}/: HttpError: Not Found`));
  })
});
