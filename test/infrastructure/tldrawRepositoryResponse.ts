export const tldrawRepositoryResponse = [
  {
    name: ".eslintignore",
    path: ".eslintignore",
    sha: "35e915e52184ec3445969e147da0d71a5cd2a358",
    size: 38,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/.eslintignore?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/.eslintignore",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/35e915e52184ec3445969e147da0d71a5cd2a358",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/.eslintignore",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/.eslintignore?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/35e915e52184ec3445969e147da0d71a5cd2a358",
      html: "https://github.com/tldraw/tldraw/blob/main/.eslintignore",
    },
  },
  {
    name: ".eslintrc",
    path: ".eslintrc",
    sha: "11748da7caf06f5a72d0a97e15c1fc7796442938",
    size: 392,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/.eslintrc?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/.eslintrc",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/11748da7caf06f5a72d0a97e15c1fc7796442938",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/.eslintrc",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/.eslintrc?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/11748da7caf06f5a72d0a97e15c1fc7796442938",
      html: "https://github.com/tldraw/tldraw/blob/main/.eslintrc",
    },
  },
  {
    name: ".github",
    path: ".github",
    sha: "1e164efc2d651d8115c8b4ffd136f954bb0f429f",
    size: 0,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/.github?ref=main",
    html_url: "https://github.com/tldraw/tldraw/tree/main/.github",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/trees/1e164efc2d651d8115c8b4ffd136f954bb0f429f",
    download_url: null,
    type: "dir",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/.github?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/trees/1e164efc2d651d8115c8b4ffd136f954bb0f429f",
      html: "https://github.com/tldraw/tldraw/tree/main/.github",
    },
  },
  {
    name: ".gitignore",
    path: ".gitignore",
    sha: "173446d8b232f481bdd0cce3034ae9db5ffc01b6",
    size: 208,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/.gitignore?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/.gitignore",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/173446d8b232f481bdd0cce3034ae9db5ffc01b6",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/.gitignore",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/.gitignore?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/173446d8b232f481bdd0cce3034ae9db5ffc01b6",
      html: "https://github.com/tldraw/tldraw/blob/main/.gitignore",
    },
  },
  {
    name: ".husky",
    path: ".husky",
    sha: "c9fde302112dfbd0638e93cbce2da264b390159d",
    size: 0,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/.husky?ref=main",
    html_url: "https://github.com/tldraw/tldraw/tree/main/.husky",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/trees/c9fde302112dfbd0638e93cbce2da264b390159d",
    download_url: null,
    type: "dir",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/.husky?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/trees/c9fde302112dfbd0638e93cbce2da264b390159d",
      html: "https://github.com/tldraw/tldraw/tree/main/.husky",
    },
  },
  {
    name: ".npmignore",
    path: ".npmignore",
    sha: "2da193068254d6bab842a8c12188ae8ba29ec072",
    size: 143,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/.npmignore?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/.npmignore",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/2da193068254d6bab842a8c12188ae8ba29ec072",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/.npmignore",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/.npmignore?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/2da193068254d6bab842a8c12188ae8ba29ec072",
      html: "https://github.com/tldraw/tldraw/blob/main/.npmignore",
    },
  },
  {
    name: ".prettierrc",
    path: ".prettierrc",
    sha: "3fbd4fbb7c8450150dcee43163c50cb692f2d4bd",
    size: 89,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/.prettierrc?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/.prettierrc",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/3fbd4fbb7c8450150dcee43163c50cb692f2d4bd",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/.prettierrc",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/.prettierrc?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/3fbd4fbb7c8450150dcee43163c50cb692f2d4bd",
      html: "https://github.com/tldraw/tldraw/blob/main/.prettierrc",
    },
  },
  {
    name: ".vscode",
    path: ".vscode",
    sha: "51aa2da296277e705292ec1d965e86202df37a19",
    size: 0,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/.vscode?ref=main",
    html_url: "https://github.com/tldraw/tldraw/tree/main/.vscode",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/trees/51aa2da296277e705292ec1d965e86202df37a19",
    download_url: null,
    type: "dir",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/.vscode?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/trees/51aa2da296277e705292ec1d965e86202df37a19",
      html: "https://github.com/tldraw/tldraw/tree/main/.vscode",
    },
  },
  {
    name: "CODE_OF_CONDUCT.md",
    path: "CODE_OF_CONDUCT.md",
    sha: "057b269445a1b13979c858b05be2ea4b29aec079",
    size: 5362,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/CODE_OF_CONDUCT.md?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/CODE_OF_CONDUCT.md",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/057b269445a1b13979c858b05be2ea4b29aec079",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/CODE_OF_CONDUCT.md",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/CODE_OF_CONDUCT.md?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/057b269445a1b13979c858b05be2ea4b29aec079",
      html: "https://github.com/tldraw/tldraw/blob/main/CODE_OF_CONDUCT.md",
    },
  },
  {
    name: "CONTRIBUTING.md",
    path: "CONTRIBUTING.md",
    sha: "f833f5caef624ff3670bbc77e42c52a197cbc208",
    size: 5249,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/CONTRIBUTING.md?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/CONTRIBUTING.md",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/f833f5caef624ff3670bbc77e42c52a197cbc208",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/CONTRIBUTING.md",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/CONTRIBUTING.md?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/f833f5caef624ff3670bbc77e42c52a197cbc208",
      html: "https://github.com/tldraw/tldraw/blob/main/CONTRIBUTING.md",
    },
  },
  {
    name: "LICENSE.md",
    path: "LICENSE.md",
    sha: "cb442d69ab89c708892043b73fc89a0402723c6c",
    size: 1073,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/LICENSE.md?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/LICENSE.md",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/cb442d69ab89c708892043b73fc89a0402723c6c",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/LICENSE.md",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/LICENSE.md?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/cb442d69ab89c708892043b73fc89a0402723c6c",
      html: "https://github.com/tldraw/tldraw/blob/main/LICENSE.md",
    },
  },
  {
    name: "README.md",
    path: "README.md",
    sha: "79e786ce8de287e4f299e1b1a2d4d2db87f1e303",
    size: 3118,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/README.md?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/README.md",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/79e786ce8de287e4f299e1b1a2d4d2db87f1e303",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/README.md",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/README.md?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/79e786ce8de287e4f299e1b1a2d4d2db87f1e303",
      html: "https://github.com/tldraw/tldraw/blob/main/README.md",
    },
  },
  {
    name: "apps",
    path: "apps",
    sha: "b354a66b70609990062d55e5a421ab21c21179a1",
    size: 0,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/apps?ref=main",
    html_url: "https://github.com/tldraw/tldraw/tree/main/apps",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/trees/b354a66b70609990062d55e5a421ab21c21179a1",
    download_url: null,
    type: "dir",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/apps?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/trees/b354a66b70609990062d55e5a421ab21c21179a1",
      html: "https://github.com/tldraw/tldraw/tree/main/apps",
    },
  },
  {
    name: "assets",
    path: "assets",
    sha: "8666e14a827993172110d91fb54c45d74ec03ea4",
    size: 0,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/assets?ref=main",
    html_url: "https://github.com/tldraw/tldraw/tree/main/assets",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/trees/8666e14a827993172110d91fb54c45d74ec03ea4",
    download_url: null,
    type: "dir",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/assets?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/trees/8666e14a827993172110d91fb54c45d74ec03ea4",
      html: "https://github.com/tldraw/tldraw/tree/main/assets",
    },
  },
  {
    name: "examples",
    path: "examples",
    sha: "ac2e219a3d43df98630b5ca34f564898b529a98d",
    size: 0,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/examples?ref=main",
    html_url: "https://github.com/tldraw/tldraw/tree/main/examples",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/trees/ac2e219a3d43df98630b5ca34f564898b529a98d",
    download_url: null,
    type: "dir",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/examples?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/trees/ac2e219a3d43df98630b5ca34f564898b529a98d",
      html: "https://github.com/tldraw/tldraw/tree/main/examples",
    },
  },
  {
    name: "guides",
    path: "guides",
    sha: "c068f7695a3abeafa03ac09c509ab27051c408ed",
    size: 0,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/guides?ref=main",
    html_url: "https://github.com/tldraw/tldraw/tree/main/guides",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/trees/c068f7695a3abeafa03ac09c509ab27051c408ed",
    download_url: null,
    type: "dir",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/guides?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/trees/c068f7695a3abeafa03ac09c509ab27051c408ed",
      html: "https://github.com/tldraw/tldraw/tree/main/guides",
    },
  },
  {
    name: "lerna.json",
    path: "lerna.json",
    sha: "601989d4a0540d0606f42a43b0fd5f4e1129986e",
    size: 192,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/lerna.json?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/lerna.json",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/601989d4a0540d0606f42a43b0fd5f4e1129986e",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/lerna.json",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/lerna.json?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/601989d4a0540d0606f42a43b0fd5f4e1129986e",
      html: "https://github.com/tldraw/tldraw/blob/main/lerna.json",
    },
  },
  {
    name: "package.json",
    path: "package.json",
    sha: "5604e4fca5b2b490b8ebe7a3aba0e32ac99c7a35",
    size: 2764,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/package.json?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/package.json",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/5604e4fca5b2b490b8ebe7a3aba0e32ac99c7a35",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/package.json",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/package.json?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/5604e4fca5b2b490b8ebe7a3aba0e32ac99c7a35",
      html: "https://github.com/tldraw/tldraw/blob/main/package.json",
    },
  },
  {
    name: "packages",
    path: "packages",
    sha: "d51d1599ca98bc95f5c10dabe703df683b6e196d",
    size: 0,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/packages?ref=main",
    html_url: "https://github.com/tldraw/tldraw/tree/main/packages",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/trees/d51d1599ca98bc95f5c10dabe703df683b6e196d",
    download_url: null,
    type: "dir",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/packages?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/trees/d51d1599ca98bc95f5c10dabe703df683b6e196d",
      html: "https://github.com/tldraw/tldraw/tree/main/packages",
    },
  },
  {
    name: "repo-map.tldr",
    path: "repo-map.tldr",
    sha: "4732385025a86ed2c93a7b46e66816ac82517506",
    size: 43024,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/repo-map.tldr?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/repo-map.tldr",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/4732385025a86ed2c93a7b46e66816ac82517506",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/repo-map.tldr",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/repo-map.tldr?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/4732385025a86ed2c93a7b46e66816ac82517506",
      html: "https://github.com/tldraw/tldraw/blob/main/repo-map.tldr",
    },
  },
  {
    name: "setupTests.ts",
    path: "setupTests.ts",
    sha: "8bd806966ce5ece36723a06650c4e3021d5a7b67",
    size: 138,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/setupTests.ts?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/setupTests.ts",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/8bd806966ce5ece36723a06650c4e3021d5a7b67",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/setupTests.ts",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/setupTests.ts?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/8bd806966ce5ece36723a06650c4e3021d5a7b67",
      html: "https://github.com/tldraw/tldraw/blob/main/setupTests.ts",
    },
  },
  {
    name: "tsconfig.base.json",
    path: "tsconfig.base.json",
    sha: "c3c0aa03dfc000aaa3543a038dcd0670aaa5af6c",
    size: 1015,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/tsconfig.base.json?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/tsconfig.base.json",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/c3c0aa03dfc000aaa3543a038dcd0670aaa5af6c",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/tsconfig.base.json",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/tsconfig.base.json?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/c3c0aa03dfc000aaa3543a038dcd0670aaa5af6c",
      html: "https://github.com/tldraw/tldraw/blob/main/tsconfig.base.json",
    },
  },
  {
    name: "tsconfig.json",
    path: "tsconfig.json",
    sha: "764cb02129b9d1b196a15f1238f1becda017b0d0",
    size: 103,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/tsconfig.json?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/tsconfig.json",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/764cb02129b9d1b196a15f1238f1becda017b0d0",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/tsconfig.json",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/tsconfig.json?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/764cb02129b9d1b196a15f1238f1becda017b0d0",
      html: "https://github.com/tldraw/tldraw/blob/main/tsconfig.json",
    },
  },
  {
    name: "yarn.lock",
    path: "yarn.lock",
    sha: "b762622adf801fc32e7f19aad00130d780f6104f",
    size: 650519,
    url: "https://api.github.com/repos/tldraw/tldraw/contents/yarn.lock?ref=main",
    html_url: "https://github.com/tldraw/tldraw/blob/main/yarn.lock",
    git_url:
      "https://api.github.com/repos/tldraw/tldraw/git/blobs/b762622adf801fc32e7f19aad00130d780f6104f",
    download_url:
      "https://raw.githubusercontent.com/tldraw/tldraw/main/yarn.lock",
    type: "file",
    _links: {
      self: "https://api.github.com/repos/tldraw/tldraw/contents/yarn.lock?ref=main",
      git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/b762622adf801fc32e7f19aad00130d780f6104f",
      html: "https://github.com/tldraw/tldraw/blob/main/yarn.lock",
    },
  },
];

export const fileResponse = {
  name: ".gitignore",
  path: ".gitignore",
  sha: "173446d8b232f481bdd0cce3034ae9db5ffc01b6",
  size: 208,
  url: "https://api.github.com/repos/tldraw/tldraw/contents/.gitignore?ref=main",
  html_url: "https://github.com/tldraw/tldraw/blob/main/.gitignore",
  git_url:
    "https://api.github.com/repos/tldraw/tldraw/git/blobs/173446d8b232f481bdd0cce3034ae9db5ffc01b6",
  download_url:
    "https://raw.githubusercontent.com/tldraw/tldraw/main/.gitignore",
  type: "file",
  content:
    "bm9kZV9tb2R1bGVzLwpidWlsZC8KbGliLwpkaXN0Lwpkb2NzLwouaWRlYS8q\n" +
    "CgouRFNfU3RvcmUKY292ZXJhZ2UKKi5sb2cKCi52ZXJjZWwKLm5leHQKYXBw\n" +
    "cy93d3cvcHVibGljL3dvcmtib3gtKgphcHBzL3d3dy9wdWJsaWMvd29ya2Vy\n" +
    "LSoKYXBwcy93d3cvcHVibGljL3N3LmpzCmFwcHMvd3d3L3B1YmxpYy9zdy5q\n" +
    "cy5tYXAKLmVudgpmaXJlYmFzZS5jb25maWcuKg==\n",
  encoding: "base64",
  _links: {
    self: "https://api.github.com/repos/tldraw/tldraw/contents/.gitignore?ref=main",
    git: "https://api.github.com/repos/tldraw/tldraw/git/blobs/173446d8b232f481bdd0cce3034ae9db5ffc01b6",
    html: "https://github.com/tldraw/tldraw/blob/main/.gitignore",
  },
};

export const notFoundResponse = {
  message: "Not Found",
  documentation_url:
    "https://docs.github.com/rest/reference/repos#get-repository-content",
};
